# recipe-app-api-proxy

recipe app proxy application
NGINX proxy app for our app API

## Usage

### Environment variables

* `LISTEN_PORT` - Port to listen in (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (defualt: `9000`)